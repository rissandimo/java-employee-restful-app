package com.rissandimo.ChadSpringBootRestCrud.rest;

import com.rissandimo.ChadSpringBootRestCrud.entity.Employee;
import com.rissandimo.ChadSpringBootRestCrud.service.EmployeeService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeRestController
{
    private EmployeeService employeeService;

    public EmployeeRestController(EmployeeService employeeService)
    {
        this.employeeService = employeeService;
    }

    @GetMapping("/getEmployees")
    public List<Employee> getEmployees()
    {
        return employeeService.getListOfEmployees();
    }

    @GetMapping("/getEmployees/{employeeId}")
    public Employee getEmployeeById(@PathVariable int employeeId)
    {
        Employee employee = employeeService.findEmployeeById(employeeId);

        if(employee == null) throw new RuntimeException("The employee with id: " + employeeId + " was not found.");
        return employee;
    }

    @PostMapping("/employees")
    public Employee saveNewEmployee(@RequestBody Employee newEmployee)
    {
        newEmployee.setId(0); // 0 -> new employee, just in case they pass an id in JSON
        employeeService.saveEmployee(newEmployee);
        return newEmployee;
    }

    @PutMapping("/employees")
    public Employee updateEmployee(@RequestBody Employee updatedEmployee) // INFO will come in AS JSON in RequestBody
    {
        employeeService.saveEmployee(updatedEmployee);
        return updatedEmployee;
    }

    @DeleteMapping("/employees/{employeeId}")
    public String deleteEmployeeById(@PathVariable int employeeId) // bind PathVariable to employeeId we get from JSON
    {
        Employee employee = employeeService.findEmployeeById(employeeId);

        if(employee == null)
            throw new RuntimeException("Employee with id: " + employeeId + " not found.");

        employeeService.deleteEmployeeById(employeeId);

        return "Employee with id: " + employeeId + " has been deleted";
    }
}
