package com.rissandimo.ChadSpringBootRestCrud.service;

import com.rissandimo.ChadSpringBootRestCrud.dao.EmployeeDao;
import com.rissandimo.ChadSpringBootRestCrud.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Delegates the calls to the DAO
 */
@Service
public class EmployeeServiceImpl implements EmployeeService
{
    private EmployeeDao employeeDao;

    /**
     * This constructor takes in a EmployeeDao (Repository/Database)
     * @Qualifier - 2 beans are available to be consumed - the one specified and EmployeeDaoImpl
     * Both are using EntityManager as the data source
     * @param employeeDao Database to use
     */
    @Autowired
    public EmployeeServiceImpl(@Qualifier("employeeDaoImplJpa") EmployeeDao employeeDao)
    {
        this.employeeDao = employeeDao;
    }

    @Override
    @Transactional
    public List<Employee> getListOfEmployees()
    {
        return employeeDao.getListOfEmployees();
    }

    @Override
    @Transactional
    public Employee findEmployeeById(int employeeId)
    {
        return employeeDao.findEmployeeById(employeeId);
    }

    @Override
    @Transactional
    public void saveEmployee(Employee employee)
    {
        employeeDao.saveEmployee(employee);
    }

    @Override
    @Transactional
    public Employee deleteEmployeeById(int employeeId)
    {
        Employee employee = findEmployeeById(employeeId);
        employeeDao.deleteEmployeeById(employeeId);
        return employee;
    }
}
