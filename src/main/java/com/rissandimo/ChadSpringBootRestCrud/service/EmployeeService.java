package com.rissandimo.ChadSpringBootRestCrud.service;

import com.rissandimo.ChadSpringBootRestCrud.entity.Employee;

import java.util.List;

public interface EmployeeService
{
    List<Employee> getListOfEmployees();

    Employee findEmployeeById(int id);

    void saveEmployee(Employee employee);

    Employee deleteEmployeeById(int id);
}
