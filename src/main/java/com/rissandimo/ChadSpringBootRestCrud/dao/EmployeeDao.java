package com.rissandimo.ChadSpringBootRestCrud.dao;

import com.rissandimo.ChadSpringBootRestCrud.entity.Employee;

import java.util.List;

public interface EmployeeDao
{
    List getListOfEmployees();

    Employee findEmployeeById(int employeeId);

    void saveEmployee(Employee employee);

    void deleteEmployeeById(int employeeId);
}
