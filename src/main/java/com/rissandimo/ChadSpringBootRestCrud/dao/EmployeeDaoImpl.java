package com.rissandimo.ChadSpringBootRestCrud.dao;

import com.rissandimo.ChadSpringBootRestCrud.entity.Employee;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao
{

    private EntityManager entityManager;

    @Autowired
    public EmployeeDaoImpl(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    @Override
    public List<Employee> getListOfEmployees()
    {
        //This is gets the current session in the background
        Session session = entityManager.unwrap(Session.class);
        Query<Employee> query = session.createQuery("from Employee", Employee.class);

         return query.getResultList();
    }

    @Override
    public Employee findEmployeeById(int employeeId)
    {
        Session session = entityManager.unwrap(Session.class);
        return session.get(Employee.class, employeeId);
    }

    @Override
    public void saveEmployee(Employee employee)
    {
        Session session = entityManager.unwrap(Session.class);
        session.saveOrUpdate(employee); // id not set explicitly to 0 -> update
    }

    @Override
    public void deleteEmployeeById(int id)
    {
        Session session = entityManager.unwrap(Session.class);
        Query<Employee> query = session.createQuery("delete from Employee where id=:employeeId");
        query.setParameter("employeeId", id);
        query.executeUpdate();
    }
}
