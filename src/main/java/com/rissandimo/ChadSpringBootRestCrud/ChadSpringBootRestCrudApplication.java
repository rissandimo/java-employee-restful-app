package com.rissandimo.ChadSpringBootRestCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChadSpringBootRestCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChadSpringBootRestCrudApplication.class, args);
	}

}
